package s3.rocket.gui.swing.fuel;

import java.util.EventListener;

public interface IFuelSolutionListener extends EventListener {
	public void solutionCalculated(FuelSolutionEvent event);
}
