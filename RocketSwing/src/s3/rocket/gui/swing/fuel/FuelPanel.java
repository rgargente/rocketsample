package s3.rocket.gui.swing.fuel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;

import net.miginfocom.swing.MigLayout;
import s3.rocket.gui.swing.DoubleInputVerifier;
import s3.rocketlib.TsiolkovskyEquation;

@SuppressWarnings("serial")
public class FuelPanel extends JPanel  implements FocusListener{
	private JTextField textFieldVe;
	private JTextField textFieldDeltav;
	private JTextField textFieldM0;
	private JButton btnSolve;
	private JLabel lblSolution;
	
	
	private DoubleInputVerifier doubleInputVerifier = new DoubleInputVerifier();
	
	private EventListenerList listenerList = new EventListenerList();

	/**
	 * Create the panel.
	 */
	public FuelPanel() {
		setLayout(new MigLayout("", "[][grow]", "[][][][][][]"));
		
		JLabel lblEffExhaustVelocity = new JLabel("Eff. Exhaust Velocity");
		add(lblEffExhaustVelocity, "cell 0 0,alignx trailing");
		
		textFieldVe = new JTextField();
		textFieldVe.setText("2.5");
		add(textFieldVe, "flowx,cell 1 0");
		textFieldVe.setColumns(10);
		textFieldVe.setInputVerifier(doubleInputVerifier);
		textFieldVe.addFocusListener(this);
		
		JLabel lblDeltav = new JLabel("Delta-v");
		add(lblDeltav, "cell 0 1,alignx trailing");
		
		textFieldDeltav = new JTextField();
		textFieldDeltav.setText("1");
		add(textFieldDeltav, "flowx,cell 1 1");
		textFieldDeltav.setColumns(10);
		textFieldDeltav.setInputVerifier(doubleInputVerifier);
		textFieldDeltav.addFocusListener(this);
		
		JLabel lblInitialMass = new JLabel("Initial mass");
		add(lblInitialMass, "cell 0 2,alignx trailing");
		
		JLabel lblKms = new JLabel("km/s");
		add(lblKms, "cell 1 0");
		
		JLabel lblKms_1 = new JLabel("km/s");
		add(lblKms_1, "cell 1 1");
		
		textFieldM0 = new JTextField();
		textFieldM0.setText("2000");
		add(textFieldM0, "flowx,cell 1 2");
		textFieldM0.setColumns(10);
		textFieldM0.setInputVerifier(doubleInputVerifier);
		textFieldM0.addFocusListener(this);
		
		JLabel lblKg = new JLabel("kg");
		add(lblKg, "cell 1 2");
		
		btnSolve = new JButton("Solve");
		btnSolve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				solve();
			}
		});
		add(btnSolve, "cell 1 4");
		
		lblSolution = new JLabel("");
		add(lblSolution, "cell 1 5");

	}
	
	private void solve() {
		double ve = Double.parseDouble(textFieldVe.getText());
		double m0 = Double.parseDouble(textFieldM0.getText());
		double deltav = Double.parseDouble(textFieldDeltav.getText());
		double fuel = TsiolkovskyEquation.solveBurntFuel(ve, m0, deltav);
		lblSolution.setText(DecimalFormat.getInstance().format(fuel) + " kgs");
		fireSolutionEvent(ve, m0, deltav, fuel);
	}

	private void fireSolutionEvent(double ve, double m0, double deltav,
			double fuel) {
		FuelSolutionEvent event = null;
		for (IFuelSolutionListener l: listenerList.getListeners(IFuelSolutionListener.class)) {
			if (event == null)
				event = new FuelSolutionEvent(this, ve, m0, deltav, fuel);
			l.solutionCalculated(event);
		}
	}

	public void addSolutionListener(IFuelSolutionListener solutionListener) {
		listenerList.add(IFuelSolutionListener.class, solutionListener);
	}
	
	public void removeSolutionListener(IFuelSolutionListener solutionListener) {
		listenerList.remove(IFuelSolutionListener.class, solutionListener);
	}
	
	@Override
	public void focusGained(FocusEvent e) {
	}

	@Override
	public void focusLost(FocusEvent e) {
		btnSolve.setEnabled(!textFieldVe.getText().isEmpty() && !textFieldM0.getText().isEmpty() && !textFieldDeltav.getText().isEmpty());  
	}

}
