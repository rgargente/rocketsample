package s3.rocket.gui.swing.fuel;

import java.util.EventObject;

@SuppressWarnings("serial")
public class FuelSolutionEvent extends EventObject {
	private double ve;
	private double m0;
	private double deltav;
	private double fuel;

	public FuelSolutionEvent(Object source, double ve, double m0,
			double deltav, double fuel) {
		super(source);
		this.ve = ve;
		this.m0 = m0;
		this.deltav = deltav;
		this.fuel = fuel;
	}
	
	public double getVe() {
		return ve;
	}

	public double getM0() {
		return m0;
	}

	public double getDeltav() {
		return deltav;
	}


	public double getFuel() {
		return fuel;
	}
	

}
