package s3.rocket.gui.swing.deltav;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import s3.rocket.gui.swing.DoubleInputVerifier;
import s3.rocketlib.TsiolkovskyEquation;

@SuppressWarnings("serial")
public class DeltavPanel extends JPanel implements FocusListener {
	private JTextField textFieldVe;
	private JTextField textFieldM0;
	private JTextField textFieldM1;
	private JButton btnSolve;
	private JLabel lblSolution;

	private DoubleInputVerifier doubleInputVerifier = new DoubleInputVerifier();

	/**
	 * Create the panel.
	 */
	public DeltavPanel() {
		setLayout(new MigLayout("", "[][grow]", "[][][][][][]"));

		JLabel label = new JLabel("Eff. Exhaust Velocity");
		add(label, "cell 0 0,alignx trailing");

		textFieldVe = new JTextField();
		textFieldVe.setText("2.5");
		textFieldVe.setColumns(10);
		add(textFieldVe, "flowx,cell 1 0");
		textFieldVe.setInputVerifier(doubleInputVerifier);
		textFieldVe.addFocusListener(this);

		JLabel label_1 = new JLabel("km/s");
		add(label_1, "cell 1 0");

		JLabel label_2 = new JLabel("Initial mass");
		add(label_2, "cell 0 1,alignx trailing");

		textFieldM0 = new JTextField();
		textFieldM0.setText("2000");
		textFieldM0.setColumns(10);
		add(textFieldM0, "flowx,cell 1 1");
		textFieldM0.setInputVerifier(doubleInputVerifier);
		textFieldM0.addFocusListener(this);

		JLabel label_3 = new JLabel("kg");
		add(label_3, "cell 1 1");

		JLabel lblFinalMass = new JLabel("Final mass");
		add(lblFinalMass, "cell 0 2,alignx trailing");

		textFieldM1 = new JTextField();
		textFieldM1.setText("1800");
		add(textFieldM1, "flowx,cell 1 2");
		textFieldM1.setColumns(10);
		textFieldM1.setInputVerifier(doubleInputVerifier);
		textFieldM1.addFocusListener(this);

		JLabel lblKg = new JLabel("kg");
		add(lblKg, "cell 1 2");

		btnSolve = new JButton("Solve");
		btnSolve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				solve();
			}
		});
		add(btnSolve, "cell 1 4");

		lblSolution = new JLabel("");
		add(lblSolution, "cell 1 5");

	}

	private void solve() {
		double ve = Double.parseDouble(textFieldVe.getText());
		double m0 = Double.parseDouble(textFieldM0.getText());
		double m1 = Double.parseDouble(textFieldM1.getText());
		double deltav = TsiolkovskyEquation.solveDeltav(ve, m0, m1);
		lblSolution.setText(DecimalFormat.getInstance().format(deltav)
				+ " km/s");
		fireSolutionEvent(ve, m0, m1, deltav);
	}

	private void fireSolutionEvent(double ve, double m0, double m1,
			double deltav) {
		DeltavSolutionEvent event = null;
		for (IDeltavSolutionListener l : listenerList
				.getListeners(IDeltavSolutionListener.class)) {
			if (event == null)
				event = new DeltavSolutionEvent(this, ve, m0, m1, deltav);
			l.solutionCalculated(event);
		}
	}

	public void addSolutionListener(IDeltavSolutionListener solutionListener) {
		listenerList.add(IDeltavSolutionListener.class, solutionListener);
	}

	public void removeSolutionListener(IDeltavSolutionListener solutionListener) {
		listenerList.remove(IDeltavSolutionListener.class, solutionListener);
	}

	@Override
	public void focusGained(FocusEvent e) {
	}

	@Override
	public void focusLost(FocusEvent e) {
		btnSolve.setEnabled(!textFieldVe.getText().isEmpty() && !textFieldM0.getText().isEmpty() && !textFieldM1.getText().isEmpty());  
	}

}
