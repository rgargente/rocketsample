package s3.rocket.gui.swing.deltav;

import java.util.EventObject;

@SuppressWarnings("serial")
public class DeltavSolutionEvent extends EventObject {
	private double ve;
	private double m0;
	private double m1;
	private double deltav;

	public DeltavSolutionEvent(Object source, double ve, double m0, double m1,
			double deltav) {
		super(source);
		this.ve = ve;
		this.m0 = m0;
		this.m1 = m1;
		this.deltav = deltav;
	}

	public double getVe() {
		return ve;
	}

	public double getM0() {
		return m0;
	}

	public double getM1() {
		return m1;
	}

	public double getDeltav() {
		return deltav;
	}

}
