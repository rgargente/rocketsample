package s3.rocket.gui.swing.deltav;

import java.util.EventListener;

public interface IDeltavSolutionListener extends EventListener {
	public void solutionCalculated(DeltavSolutionEvent event);
}
