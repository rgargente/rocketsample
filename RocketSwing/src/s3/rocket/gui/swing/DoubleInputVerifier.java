package s3.rocket.gui.swing;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class DoubleInputVerifier extends InputVerifier {

	@Override
	public boolean verify(JComponent input) {
		boolean verified = false;
		if (input instanceof JTextField) {
			String inputText = ((JTextField) input).getText();
			if (inputText.isEmpty()) {
				verified = true;
			} else {
				try {
					Double.parseDouble(inputText);
					verified = true;
				} catch (NumberFormatException e) {
				}
			}
		}
		return verified;
	}

}
