package s3.rocket.gui.swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.text.DecimalFormat;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import s3.rocket.gui.swing.deltav.DeltavPanel;
import s3.rocket.gui.swing.deltav.DeltavSolutionEvent;
import s3.rocket.gui.swing.deltav.IDeltavSolutionListener;
import s3.rocket.gui.swing.fuel.FuelPanel;
import s3.rocket.gui.swing.fuel.FuelSolutionEvent;
import s3.rocket.gui.swing.fuel.IFuelSolutionListener;

public class RocketSwingApp implements IDeltavSolutionListener,
		IFuelSolutionListener {

	private JFrame frmTsiolkovskyEquationSolver;

	private DeltavPanel deltavPanel;
	private FuelPanel fuelPanel;
	private JTextArea textAreaOutput;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RocketSwingApp window = new RocketSwingApp();
					window.frmTsiolkovskyEquationSolver.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RocketSwingApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTsiolkovskyEquationSolver = new JFrame();
		frmTsiolkovskyEquationSolver.setTitle("Tsiolkovsky Equation Solver");
		frmTsiolkovskyEquationSolver.setBounds(100, 100, 550, 300);
		frmTsiolkovskyEquationSolver
				.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JSplitPane splitPane = new JSplitPane();
		frmTsiolkovskyEquationSolver.getContentPane().add(splitPane);

		deltavPanel = new DeltavPanel();
		deltavPanel.addSolutionListener(this);
		fuelPanel = new FuelPanel();
		fuelPanel.addSolutionListener(this);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		splitPane.setLeftComponent(tabbedPane);
		tabbedPane.add(deltavPanel);
		tabbedPane.setTitleAt(0, "Delta-v");
		tabbedPane.add(fuelPanel);
		tabbedPane.setTitleAt(1, "Fuel");

		JPanel panelOutput = new JPanel();
		panelOutput.setBorder(new TitledBorder(null, "Output log",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelOutput.setLayout(new BorderLayout(0, 0));
		splitPane.setRightComponent(panelOutput);

		textAreaOutput = new JTextArea();
		textAreaOutput.setFont(new Font("Dialog", Font.PLAIN, 11));
		textAreaOutput.setEditable(false);
		textAreaOutput.setColumns(15);
		JScrollPane scrollPane = new JScrollPane(textAreaOutput);
		panelOutput.add(scrollPane, BorderLayout.CENTER);

	}

	@Override
	public void solutionCalculated(DeltavSolutionEvent event) {
		StringBuilder output = new StringBuilder();
		output.append("Delta-v calculated for: \n");
		output.append("Ve: " + event.getVe() + "\n");
		output.append("M0: " + event.getM0() + "\n");
		output.append("M1: " + event.getM1() + "\n");
		output.append("DELTA-V: " + DecimalFormat.getInstance().format(event.getDeltav())  + "\n");
		output.append("******\n");
		textAreaOutput.append(output.toString());
	}

	@Override
	public void solutionCalculated(FuelSolutionEvent event) {
		StringBuilder output = new StringBuilder();
		output.append("Fuel calculated for: \n");
		output.append("Ve: " + event.getVe() + "\n");
		output.append("M0: " + event.getM0() + "\n");
		output.append("Delta-v: " + event.getDeltav() + "\n");
		output.append("FUEL: " + DecimalFormat.getInstance().format(event.getFuel()) + "\n");
		output.append("******\n");
		textAreaOutput.append(output.toString());
	}

}
