package s3.rocket.gui.rocketeditor;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.FlowLayout;

import javax.swing.JButton;

import s3.rocket.gui.rocketeditor.stagepanel.IStagesChangeListener;
import s3.rocket.gui.rocketeditor.stagepanel.StageChangeAction;
import s3.rocket.gui.rocketeditor.stagepanel.StagePanel;
import s3.rocket.gui.rocketeditor.stagepanel.StagesChangeEvent;
import s3.rocketlib.rocket.Rocket;
import s3.rocketlib.rocket.Stage;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;

import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.xml.bind.JAXBException;

@SuppressWarnings("serial")
public class RocketPanel extends JPanel implements IStagesChangeListener {

	private static final String ROCKETS_PATH = System.getProperty("user.dir")
			+ "/rockets";

	private JTextField txtName;
	private JPanel stagesPanel;

	private Rocket rocket = new Rocket();

	/**
	 * Create the panel.
	 */
	public RocketPanel() {
		setLayout(new BorderLayout(4, 4));

		JPanel topPanel = new JPanel();
		add(topPanel, BorderLayout.NORTH);
		topPanel.setLayout(new BorderLayout(0, 0));

		JLabel lblName = new JLabel("Name:");
		topPanel.add(lblName, BorderLayout.WEST);

		txtName = new JTextField();
		topPanel.add(txtName, BorderLayout.CENTER);
		txtName.setColumns(10);

		JPanel bottomPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) bottomPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		add(bottomPanel, BorderLayout.SOUTH);

		JButton btnAddStage = new JButton("Add Stage");
		btnAddStage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewStage();
			}
		});

		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				load();
			}
		});
		bottomPanel.add(btnLoad);
		bottomPanel.add(btnAddStage);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				save();
			}
		});
		bottomPanel.add(btnSave);

		stagesPanel = new JPanel();
		stagesPanel.setBorder(null);
		JScrollPane stagesScrollPane = new JScrollPane(stagesPanel);
		stagesPanel.setLayout(new BoxLayout(stagesPanel, BoxLayout.Y_AXIS));
		add(stagesScrollPane, BorderLayout.CENTER);

		refreshStagesPanel();
	}

	private void addNewStage() {
		rocket.addStage();
		refreshStagesPanel();
	}

	private void addStagePanel(Stage stage) {
		StagePanel stagePanel = new StagePanel(stage);
		stagePanel.addStageRemoveListener(this);
		stagesPanel.add(stagePanel, 0);
		updateUI();
	}

	@Override
	public void stagesChanged(StagesChangeEvent e) {
		StageChangeAction action = e.getAction();
		switch (action) {
		case Remove:
			rocket.removeStage(e.getStage());
			break;
		case MoveUp:
			rocket.moveStageUp(e.getStage());
			break;
		case MoveDown:
			rocket.moveStageDown(e.getStage());
			break;
		}
		updateStagesData();
		refreshStagesPanel();
	}

	private void refreshStagesPanel() {
		stagesPanel.removeAll();
		for (Stage stage : rocket.getStages()) {
			addStagePanel(stage);
		}
		((StagePanel) stagesPanel.getComponent(0)).getMoveUpButton()
				.setEnabled(false);
		((StagePanel) stagesPanel
				.getComponent(stagesPanel.getComponentCount() - 1))
				.getMoveDownButton().setEnabled(false);
		if (rocket.getStages().size() == 1) {
			((StagePanel) stagesPanel.getComponent(0)).getRemoveButton().setEnabled(false);
		}
	}

	private void updateStagesData() {
		for (Component stagePanel : stagesPanel.getComponents()) {
			((StagePanel) stagePanel).updateStage();
		}
	}

	private void load() {
		JFileChooser fc = new JFileChooser(ROCKETS_PATH);
		if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			try {
				rocket = Rocket.loadXmlFile(fc.getSelectedFile());
				txtName.setText(rocket.getName());
				refreshStagesPanel();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		}
	}

	private void save() {
		rocket.setName(txtName.getText());
		updateStagesData();
		try {
			rocket.save(ROCKETS_PATH);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

}
