package s3.rocket.gui.rocketeditor;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.border.EmptyBorder;

public class RockedEditorApp {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RockedEditorApp window = new RockedEditorApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RockedEditorApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 463);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		RocketPanel rocketPanel = new RocketPanel();
		rocketPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.getContentPane().add(rocketPanel, BorderLayout.CENTER);
	}

}
