package s3.rocket.gui.rocketeditor.stagepanel;

public enum StageChangeAction {
	MoveUp,
	MoveDown,
	Remove
}
