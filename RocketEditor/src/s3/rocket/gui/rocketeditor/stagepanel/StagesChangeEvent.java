package s3.rocket.gui.rocketeditor.stagepanel;

import java.util.EventObject;

import s3.rocketlib.rocket.Stage;

@SuppressWarnings("serial")
public class StagesChangeEvent extends EventObject {

	private Stage stage;
	private StageChangeAction action;

	public StagesChangeEvent(Object source, Stage stage, StageChangeAction action) {
		super(source);
		this.stage = stage;
		this.action = action;
	}

	public Stage getStage() {
		return stage;
	}

	public StageChangeAction getAction() {
		return action;
	}
}
