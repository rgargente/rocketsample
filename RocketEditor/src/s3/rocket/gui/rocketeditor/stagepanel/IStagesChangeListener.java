package s3.rocket.gui.rocketeditor.stagepanel;

import java.util.EventListener;

public interface IStagesChangeListener extends EventListener {
	public void stagesChanged(StagesChangeEvent e);
}
