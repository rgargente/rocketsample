package s3.rocket.gui.rocketeditor.stagepanel;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import s3.rocketlib.rocket.Stage;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class StagePanel extends JPanel {
	private JTextField txtEmptyMass;
	private JTextField txtGrossMass;
	private JTextField txtSpecificImpulse;
	private TitledBorder border;
	
	/* Buttons are made visible to allow easy enabling and disabling. Custom events may be more elegant. */
	
	private JButton btnMoveDown;
	
	public JButton getMoveDownButton() {
		return btnMoveDown;
	}
	
	private JButton btnMoveUp;
	
	public JButton getMoveUpButton() {
		return btnMoveUp;
	}
	
	JButton btnRemove;
	
	public JButton getRemoveButton() {
		return btnRemove;
	}
	
	private Stage stage;

	/**
	 * Create the panel.
	 */
	public StagePanel(Stage stage) {
		border = new TitledBorder(null, "Stage", TitledBorder.LEADING, TitledBorder.TOP, null, null);
		setBorder(border);
		setLayout(new MigLayout("", "[][grow][][]", "[][][]"));
		
		JLabel lblEmptyMass = new JLabel("Empty mass");
		add(lblEmptyMass, "cell 0 0,alignx trailing");
		
		txtEmptyMass = new JTextField();
		add(txtEmptyMass, "cell 1 0,growx");
		txtEmptyMass.setColumns(10);
		
		JLabel lblKg = new JLabel("kg");
		add(lblKg, "cell 2 0");
		
		btnMoveUp = new JButton("Move Up");
		btnMoveUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fireEvent(StageChangeAction.MoveUp);
			}
		});
		add(btnMoveUp, "cell 3 0,growx");
		
		JLabel lblGrossMass = new JLabel("Gross mass");
		add(lblGrossMass, "cell 0 1,alignx trailing");
		
		txtGrossMass = new JTextField();
		add(txtGrossMass, "cell 1 1,growx");
		txtGrossMass.setColumns(10);
		
		JLabel lblKg_1 = new JLabel("kg");
		add(lblKg_1, "cell 2 1");
		
		btnMoveDown = new JButton("Move Down");
		btnMoveDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(StageChangeAction.MoveDown);
			}
		});
		add(btnMoveDown, "cell 3 1,growx");
		
		JLabel lblSpecificImpulse = new JLabel("Specific impulse");
		add(lblSpecificImpulse, "cell 0 2,alignx trailing");
		
		txtSpecificImpulse = new JTextField();
		add(txtSpecificImpulse, "cell 1 2,growx");
		txtSpecificImpulse.setColumns(10);
		
		JLabel lblSeconds = new JLabel("seconds");
		add(lblSeconds, "cell 2 2");
		
		btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				removeStage();
			}
		});
		add(btnRemove, "cell 3 2,growx");

		setStage(stage);
	}
	
	private void removeStage() {
		fireEvent(StageChangeAction.Remove);
	}
	
	private void fireEvent(StageChangeAction action) {
		StagesChangeEvent event = null;
		for (IStagesChangeListener l : listenerList.getListeners(IStagesChangeListener.class)) {
			if (event == null)
				event = new StagesChangeEvent(this, stage, action);
			l.stagesChanged(event);
		}
	}
	
	public void addStageRemoveListener(IStagesChangeListener listener) {
		listenerList.add(IStagesChangeListener.class, listener);
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
		String title = String.format("Stage %d", stage.getNumber());
		border.setTitle(title);
		txtEmptyMass.setText(Double.toString(stage.getEmptyMass()));
		txtGrossMass.setText(Double.toString(stage.getGrossMass()));
		txtSpecificImpulse.setText(Double.toString(stage.getSpecificImpulse()));
	}
	
	public void updateStage() {
		stage.setEmptyMass(Double.parseDouble(txtEmptyMass.getText()));
		stage.setGrossMass(Double.parseDouble(txtGrossMass.getText()));
		stage.setSpecificImpulse(Double.parseDouble(txtSpecificImpulse.getText()));
	}
	
}
