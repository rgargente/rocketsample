import sys
import math
from PyPlotter import Graph 
from PyPlotter import awtGfx
sys.path.append("rocketlib.jar")
from s3.rocketlib  import TsiolkovskyEquation as eq


# Get the input
print "Tsiolkovsky plotter"
print "First, input some mission parameters:"
ve = float(raw_input("Effective exhaust velocity, in km/s (about 4.4 km/s for Space Shuttle): "))
deltav = float(raw_input("Delta-v for the mission, in km/s (about 10 km/s for going from Low Earth Orbit to Mars): "))

# Init window
win = awtGfx.Window(title="Tsiolkovsky Plotter")

# Plot 1
print "Let's see how much fuel do we need for different payloads."
print "As you can see, the fuel needed follows a linear progression as we add payload."
m1 = 100 # min mass
m2 = 2000 # max mass
f2 = eq.solveFuelNeeded(ve, deltav, m2) # max fuel, we need the value to draw the cartesian graph
grFuel = Graph.Cartesian(win, m1, 0, m2, f2, 
	title="Don't close this window, watch your terminal", xaxis="Spacecraft mass (kg)", yaxis="Fuel mass (kg)")    
for x in grFuel.xaxisSteps(m1, m2):
	grFuel.addValue("Fuel (kg)", x, eq.solveFuelNeeded(ve, deltav, x))
win.refresh()
raw_input("Press Enter to continue...")

# Plot 2
print "Now let's see how, for a given spacecraft mass, delta-v obtained from fuel follows a logarithmic progression.."
m = float(raw_input("Spacecraft mass, in kg (Curiosity is about 1000 kg): "))
f2 = 3e6
dv2 = eq.solveDeltav(ve, f2+m, m)
grDeltav = Graph.Cartesian(win, 0, 0, f2, dv2, 
	title="Delta-v obtained for each unit of fuel is less as we add more fuel", xaxis="Fuel mass (kg)", yaxis="Delta-v (km/s)")    
for x in grDeltav.xaxisSteps(0, f2):
	grDeltav.addValue("Delta-v (km/s)", x, eq.solveDeltav(ve, x+m, m))
win.waitUntilClosed()






