/**
 * 
 */
package s3.rocketlib;

/**
 * @author rafa
 *
 */
public class TsiolkovskyEquation {
	
	public static double solveDeltav(double ve, double m0, double m1) {
		return ve * Math.log(m0/m1); 
	}
	
	public static double solveBurntFuel(double ve, double m0, double deltav) {
		return m0 - (m0/Math.exp(deltav/ve));
	}
	
	public static double solveFuelNeeded(double ve, double deltav, double spacecraftMass) {
		return spacecraftMass * (Math.exp(deltav/ve) - 1);
	}

}
