package s3.rocketlib.rocket;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "stage")
@XmlType(propOrder = { "number", "emptyMass", "grossMass", "specificImpulse" })
public class Stage {
	private double emptyMass = 0;

	public double getEmptyMass() {
		return emptyMass;
	}

	@XmlElement
	public void setEmptyMass(double emptyMass) {
		this.emptyMass = emptyMass;
	}

	private double grossMass = 0;

	public double getGrossMass() {
		return grossMass;
	}

	@XmlElement
	public void setGrossMass(double grossMass) {
		this.grossMass = grossMass;
	}

	private double specificImpulse = 0; // In seconds

	public double getSpecificImpulse() {
		return specificImpulse;
	}

	@XmlElement
	public void setSpecificImpulse(double specificImpulse) {
		this.specificImpulse = specificImpulse;
	}

	public int getNumber() {
		return number;
	}
	
	@XmlElement
	public void setNumber(int number) {
		this.number = number;
	}

	private int number;

	public Stage(int number) {
		setNumber(number);
	}

	
	/**
	 * You should use the  Stage(int number) constructor. This is needed for JAXB
	 */
	public Stage() {
		setNumber(number);
	}
}
