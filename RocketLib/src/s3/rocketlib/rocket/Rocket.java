package s3.rocketlib.rocket;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Rocket {
	private String name = "";

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	private ArrayList<Stage> stages = new ArrayList<Stage>();

	public ArrayList<Stage> getStages() {
		return stages;
	}

	@XmlElementWrapper
	@XmlElement
	public void setStages(ArrayList<Stage> stages) {
		this.stages = stages;
	}

	public Rocket() {
		addStage();
	}

	public Stage addStage() {
		Stage stage = new Stage(stages.size() + 1);
		stages.add(stage);
		return stage;
	}

	public void save(String path) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Rocket.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		File file = new File(path);
		file.mkdirs();
		file = new File(String.format("%s/%s.xml", path, name));
		m.marshal(this, file);
	}

	public static Rocket loadXmlFile(File file) throws JAXBException,
			FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(Rocket.class);
		Unmarshaller um = context.createUnmarshaller();
		Rocket rocket = (Rocket) um.unmarshal(new FileReader(file));
		// Sort the stages by stage number.
		Collections.sort(rocket.stages, new Comparator<Stage>() {
			@Override
			public int compare(Stage s1, Stage s2) {
				return s1.getNumber() - s2.getNumber();
			}
		});
		return rocket;
	}

	public void removeStage(Stage stage) {
		stages.remove(stage);
		renumberStages();
	}

	private void renumberStages() {
		for (int i = 0; i < stages.size(); i++) {
			stages.get(i).setNumber(i + 1);
		}
	}

	public void moveStageDown(Stage stage) {
		int i = stages.indexOf(stage);
		Stage other = stages.get(i-1);
		stage.setNumber(stage.getNumber() - 1);
		other.setNumber(other.getNumber() + 1);
		stages.set(i-1, stage);
		stages.set(i, other);
	}

	public void moveStageUp(Stage stage) {
		int i = stages.indexOf(stage);
		Stage other = stages.get(i+1);
		stage.setNumber(stage.getNumber() + 1);
		other.setNumber(other.getNumber() - 1);
		stages.set(i+1, stage);
		stages.set(i, other);
	}
}
