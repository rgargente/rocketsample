package s3.rocketlib;

import static org.junit.Assert.*;

import org.junit.Test;

public class TsiolkovskyEquationTest {

	private double ve = 2.5;
	private double m0 = 2;
	private double m1 = 1.8;
	private double burnt = m0 - m1;
	private double deltav = 0.2634;

	@Test
	public void testSolveDeltav() {
		double actual = TsiolkovskyEquation.solveDeltav(ve, m0, m1);
		assertEquals(actual, deltav, 0.0001);
	}

	@Test
	public void testSolveBurntFuel() {
		double actual = TsiolkovskyEquation.solveBurntFuel(ve, m0, deltav);
		assertEquals(actual, burnt, 0.0001);
	}
	
	@Test 
	public void testSolveFuelNeeded() {
		double actual = TsiolkovskyEquation.solveFuelNeeded(ve,  deltav, m1);
		assertEquals(0.2, actual, 0.0001);
	}

}
